from flask import Flask, render_template

app = Flask(__name__)

@app.route("/")
def home():
    return render_template("index.html")

if __name__ == "__main__":
    app.run()

# from html.parser import HTMLParser
# import pyperclip

# emailType = "verification"

# match emailType:
#     case "verification":
#         pyperclip.copy("Hey [name],\n \n Thank you for signing up to my weekly newsletter. Before we get started, you‘ll have to confirm your email address.\n \n Click on the button below to verify your email address and you‘re officially one of us!\n \n [CTA button]")
#     case "welcome":
#         pyperclip.copy("Hey [name]!\n \n Welcome to [brand name]. We are happy to have you join our community.\n \n [Brand name] goal is to create [add goal and/or mission of your brand].\n \n We promise to only send you emails [add how many times per week you will be sending an email].\n \n All our emails will offer valuable information to help you along your journey and we may occasionally recommend a product that we believe in.\n \n We hope you enjoy your time with us and, in the meantime, feel free to check our [educational resources of your brand]\n \n [Brand name]")